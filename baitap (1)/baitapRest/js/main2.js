let tinhTrungBinh = (...nums) => {
  if (nums.length) {
    let total = 0;
    for (let i of nums) {
      total += i;
    }
    return total / nums.length;
  }
};

const khoiMot = () => {
  let toan = document.getElementById("inpToan").value * 1;
  let ly = document.getElementById("inpLy").value * 1;
  let hoa = document.getElementById("inpHoa").value * 1;
  // console.log(toan, ly, hoa);
  let add = tinhTrungBinh(toan, ly, hoa);
  console.log(" add : ", add);
  // let trungBinhKhoi1 = (toan + ly + hoa) / 3;
  document.getElementById("tbKhoi1").innerHTML = add;
};
const khoiHai = () => {
  let van = document.getElementById("inpVan").value * 1;
  let su = document.getElementById("inpSu").value * 1;
  let dia = document.getElementById("inpDia").value * 1;
  let anh = document.getElementById("inpEnglish").value * 1;
  let add = tinhTrungBinh(van, su, dia, anh);
  console.log("add: ", add);
  // let trungBinhKhoi2 = (van + su + dia + anh) / 4;
  document.getElementById("tbKhoi2").innerHTML = add;
};
