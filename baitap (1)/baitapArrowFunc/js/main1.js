let listColor = [
  "pallet",
  "viridia",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

let renderColor = () => {
  let contentHTML = "";
  for (let index = 0; index < listColor.length; index++) {
    let color = listColor[index];

    contentHTML += `<button  class="color-button ${color} btn-lg btn-block"  onClick="changeColor('${color}')"></button>
    `;
  }
  document.querySelector("#colorContainer").innerHTML = contentHTML;
};
renderColor();

window.changeColor = (color) => {
  console.log("color: ", color);
  document.querySelector("#house").className = "house " + color;
  // console.log("querySelector: ", document.querySelector("#house").className);
};
